### Time Complexity Examples:

1. **O(1) - Constant Time**
   - **Explanation**: Independent of input size.
   - **Example**: Accessing an element in an array by index.
     ```python
     def get_first_element(arr):
         return arr[0]
     ```

2. **O(N) - Linear Time**
   - **Explanation**: Grows directly with input size.
   - **Example**: Finding the maximum element in an array.
     ```python
     def find_max(arr):
         max_val = arr[0]
         for num in arr:
             if num > max_val:
                 max_val = num
         return max_val
     ```

3. **O(N^2) - Quadratic Time**
   - **Explanation**: Grows with the square of the input size.
   - **Example**: Bubble sort algorithm.
     ```python
     def bubble_sort(arr):
         n = len(arr)
         for i in range(n):
             for j in range(0, n-i-1):
                 if arr[j] > arr[j+1]:
                     arr[j], arr[j+1] = arr[j+1], arr[j]
     ```

4. **O(log N) - Logarithmic Time**
   - **Explanation**: Grows slower, doubling the input size adds a constant time.
   - **Example**: Binary search in a sorted array.
     ```python
     def binary_search(arr, target):
         low, high = 0, len(arr) - 1
         while low <= high:
             mid = (low + high) // 2
             if arr[mid] == target:
                 return mid
             elif arr[mid] < target:
                 low = mid + 1
             else:
                 high = mid - 1
         return -1
     ```

5. **O(N log N) - Linearithmic Time**
   - **Explanation**: Common in efficient sorting.
   - **Example**: Merge sort algorithm.
     ```python
     def merge_sort(arr):
         if len(arr) > 1:
             mid = len(arr) // 2
             left_half = arr[:mid]
             right_half = arr[mid:]
             
             merge_sort(left_half)
             merge_sort(right_half)
             
             i = j = k = 0
             
             while i < len(left_half) and j < len(right_half):
                 if left_half[i] < right_half[j]:
                     arr[k] = left_half[i]
                     i += 1
                 else:
                     arr[k] = right_half[j]
                     j += 1
                 k += 1
             
             while i < len(left_half):
                 arr[k] = left_half[i]
                 i += 1
                 k += 1
             
             while j < len(right_half):
                 arr[k] = right_half[j]
                 j += 1
                 k += 1
     ```

6. **O(2^N) - Exponential Time**
   - **Explanation**: Grows very fast, doubling the input size doubles the time.
   - **Example**: Solving the Towers of Hanoi problem.
     ```python
     def towers_of_hanoi(n, source, target, auxiliary):
         if n == 1:
             print(f"Move disk 1 from {source} to {target}")
             return
         towers_of_hanoi(n-1, source, auxiliary, target)
         print(f"Move disk {n} from {source} to {target}")
         towers_of_hanoi(n-1, auxiliary, target, source)
     ```

7. **O(N!) - Factorial Time**
   - **Explanation**: Extremely fast growth, adding one input significantly increases time.
   - **Example**: Generating all permutations of a string.
     ```python
     def permute(s):
         if len(s) == 1:
             return [s]
         permutations = []
         for i, char in enumerate(s):
             for perm in permute(s[:i] + s[i+1:]):
                 permutations.append(char + perm)
         return permutations
     ```

### Space Complexity Examples:

1. **O(1) - Constant Space**
   - **Explanation**: Independent of input size.
   - **Example**: Swapping two variables.
     ```python
     def swap(a, b):
         temp = a
         a = b
         b = temp
     ```

2. **O(N) - Linear Space**
   - **Explanation**: Grows directly with input size.
   - **Example**: Storing the elements of an array.
     ```python
     def copy_array(arr):
         new_arr = [0] * len(arr)
         for i in range(len(arr)):
             new_arr[i] = arr[i]
         return new_arr
     ```

3. **O(N^2) - Quadratic Space**
   - **Explanation**: Grows with the square of the input size.
   - **Example**: Creating a 2D matrix of size N x N.
     ```python
     def create_matrix(n):
         matrix = [[0] * n for _ in range(n)]
         return matrix
     ```

4. **O(log N) - Logarithmic Space**
   - **Explanation**: Grows slower, doubling the input size adds a constant space.
   - **Example**: Recursive binary search (considering the call stack).
     ```python
     def binary_search(arr, low, high, target):
         if high >= low:
             mid = (high + low) // 2
             if arr[mid] == target:
                 return mid
             elif arr[mid] > target:
                 return binary_search(arr, low, mid - 1, target)
             else:
                 return binary_search(arr, mid + 1, high, target)
         else:
             return -1
     ```

5. **O(N log N) - Linearithmic Space**
   - **Explanation**: Common in efficient sorting algorithms that use divide-and-conquer strategies.
   - **Example**: Merge sort algorithm.
     ```python
     def merge_sort(arr):
         if len(arr) > 1:
             mid = len(arr) // 2
             left_half = arr[:mid]
             right_half = arr[mid:]
             
             merge_sort(left_half)
             merge_sort(right_half)
             
             i = j = k = 0
             
             while i < len(left_half) and j < len(right_half):
                 if left_half[i] < right_half[j]:
                     arr[k] = left_half[i]
                     i += 1
                 else:
                     arr[k] = right_half[j]
                     j += 1
                 k += 1
             
             while i < len(left_half):
                 arr[k] = left_half[i]
                 i += 1
                 k += 1
             
             while j < len(right_half):
                 arr[k] = right_half[j]
                 j += 1
                 k += 1
     ```

6. **O(2^N) - Exponential Space**
   - **Explanation**: Grows very fast, doubling the input size doubles the space.
   - **Example**: Recursive solution to the Towers of Hanoi problem (considering the call stack).
     ```python
     def towers_of_hanoi(n, source, target, auxiliary):
         if n == 1:
             print(f"Move disk 1 from {source} to {target}")
             return
         towers_of_hanoi(n-1, source, auxiliary, target)
         print(f"Move disk {n} from {source} to {target}")
         towers_of_hanoi(n-1, auxiliary, target, source)
     ```

7. **O(N!) - Factorial Space**
   - **Explanation**: Extremely fast growth, adding one input significantly increases space.
   - **Example**: Generating all permutations of a string (considering the space needed to store all permutations).
     ```python
     def permute(s):
         if len(s) == 1:
             return [s]
         permutations = []
         for i, char in enumerate(s):
             for perm in permute(s[:i] + s[i+1:]):
                 permutations.append(char + perm)
         return permutations
     ```